import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Todo from "./Todo"
import ApolloClient from "apollo-boost";
const client = new ApolloClient({});

const GET_ALLTODO = gql`{
  getAllTodo {
    text
    id
  } 
}`;

class Todolist extends React.Component {

    constructor(props) {
        super(props);
        this.state = {  };
    }

    refresh() { 
        client.query({
            query: GET_ALLTODO,
            fetchPolicy: 'network-only'
        }).then(response=>{
            this.setState({
                list: response.data.getAllTodo
            });
        })
    }

    render() {
        return (
            <div className="todolist">
                <button onClick={this.refresh.bind(this)}>Refresh</button>
                {
                    this.state.list && <div>{this.state.list.map(todo => <Todo todo={todo} key={todo.id}/>)}</div>
                }
                {
                    !this.state.list && <Query query={GET_ALLTODO}>{
                        ({loading, error, data}) => {
                            if(loading)  return <div>LOADING</div>
                            else if(error) return <div>{`Error: ${error}`}</div>
                            return <div>{data.getAllTodo.map(todo => <Todo todo={todo} key={todo.id}/>)}</div>
                        }
                    }</Query>
                }

            </div>
        );
    }
}

export default Todolist