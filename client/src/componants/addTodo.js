import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const SET_TODO = gql`mutation createTodo($text: String!){
    createTodo(text: $text) {
        text
        id
    }
}`;

class AddTodo extends React.Component {

    constructor(props) {
        super(props);
        this.state = { text: '' };
    }

    change(e) {
        const value = e.target.value;
        this.setState({ text: value });
    }

    send(addTodo) {
        const text = this.state.text;
        addTodo({ variables: { text } });
        this.setState({ text: '' });
    }


    render() {
        return (
            <div className="addTodo">
                <Mutation mutation={SET_TODO}>{
                    (addTodo, {data}) => 
                        <div>
                            <input
                                className='addTodo__input'
                                type='text'
                                value={this.state.text|| ''}
                                onChange={this.change.bind(this)} 
                            />
                            <button
                                className='addTodo__button'
                                onClick={this.send.bind(this, addTodo)}>
                                add
                            </button>
                        </div>
                }</Mutation>
            </div>
        );
    }
}

export default AddTodo;
