import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const DELETE_TODO = gql`mutation removeTodo($id: ID!){
    removeTodo(id: $id) {
        text
        id
    }
}`;

class Todo extends React.Component {


    remove(remove) {
        remove({ variables: { id: this.props.todo.id } })
    }

    render() {
        return (
            <div className="todo">
                {this.props.todo.text}
                <Mutation mutation={DELETE_TODO}>{
                    (remove, {data}) => 
                        <div 
                            className='todo__remove'
                            onClick={this.remove.bind(this, remove)}>remove
                        </div>
                }</Mutation>
            </div>
        );
    }
}

export default Todo;
