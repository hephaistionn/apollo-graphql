import { render } from "react-dom";
import "./app.scss";
import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import Todolist from "./componants/todolist";
import AddTodo from "./componants/addTodo";

const client = new ApolloClient({});
window.apclient = client;

render(
    <ApolloProvider client={client}>
        <div>
            <h1>TODO LIST With APOLLO</h1>
            <Todolist />
            <AddTodo />
        </div>
    </ApolloProvider>, document.getElementById("app")
);
