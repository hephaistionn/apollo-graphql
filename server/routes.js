const express = require('express');
const router = express.Router();
const path = require('path');
const indexTemplate = path.resolve(__dirname, './templates/index.ejs');

router.use('/', express.static('client/.dist'));

router.get('/test', test);
router.get('/', index);

function test(req, res) {
    res.send('test');
}

function index(req, res) {
    res.render(indexTemplate);
}

module.exports = router;