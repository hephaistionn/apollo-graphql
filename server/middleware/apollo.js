const route = require("express").Router();
const bodyParser = require("body-parser");
const { graphqlExpress, graphiqlExpress } = require("apollo-server-express");
const { makeExecutableSchema } = require("graphql-tools");
const { PubSub } = require("graphql-subscriptions");
const pubsub = new PubSub();
const { SubscriptionServer } = require('subscriptions-transport-ws');
const  { execute, subscribe } = require('graphql');

const todoList = [
    {
        text: "todo 1",
        complete: false,
        id: 0
    },{
        text: "todo 2",
        complete: false,
        id: 1
    }
];


const typeDefs = `
    type Query { 
        getAllTodo: [Todo]
        getTodo(id: ID!): Todo
    }
    type Mutation {
        createTodo(text: String!): Todo
        updateTodo(id: ID!, text: String, complete: Boolean): Todo
        removeTodo(id: ID!): [Todo]
    }
    type Subscription {
        todoUpdated: [Todo]
    }
    type Todo {  
        id: ID!
        text: String!
        complete: Boolean!
    }
  `;

// The resolvers
const resolvers = {
    Query: { 
        getAllTodo: () => todoList,
        getTodo: (obj, args) => todoList.find(todo=>parseInt(args.id, 10) === todo.id)
    },
    Mutation: {
        createTodo: (obj, args) => {
            var todo = {id: todoList.length, text: args.text, complete: false};
            todoList.push(todo);
            //pubsub.publish('todoUpdated', todoList);
            return todo;
        },
        updateTodo: (obj, args) => {
            var todo = todoList.find(todo=>parseInt(args.id, 10) === todo.id);
            todo.text = args.text;
            todo.complete = args.complete;
            //pubsub.publish('todoUpdated', todoList);
            return todo;
        },
        removeTodo: (obj, args) => {
            var index = todoList.findIndex(todo=>parseInt(args.id, 10) === todo.id);
            todoList.splice(index,1);
            //pubsub.publish('todoUpdated', todoList);
            return todoList;
        }
    }/*,
    Subscription: {
        todoUpdated: {
            subscribe: () => {pubsub.asyncIterator('todoUpdated');console.log('onTodoUpdated')}
        }
    }*/
};

// Put together a schema
const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

// The GraphQL endpoint
route.use('/graphql', bodyParser.json(), graphqlExpress({ schema, cacheControl: true,  tracing: true, }));

// GraphiQL, a visual editor for queries
route.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
    // subscriptionsEndpoint: `ws://localhost:3000/subscriptions`
}));


/*setTimeout(()=> {
    var server = require("./../server");
    console.log('----- ws ', server);
    new SubscriptionServer({
        execute,
        subscribe,
        schema
    }, {
        server: server,
        path: '/subscriptions',
    });

    pubsub.publish('todoUpdated', []);
}, 1000);*/

module.exports = route;
