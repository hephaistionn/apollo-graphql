const express = require('express');
const app = express();
const yields = require('express-yields');
const cookieParser = require('cookie-parser')();
const bodyParser = require('body-parser').json();
const middlewareError = require('./middleware/error');
const middlewareApollo = require('./middleware/apollo');
const routes = require('./routes');


// templating engine 
app.set('view engine', 'ejs');

// loading middleware
app.use(cookieParser, bodyParser);
app.use(middlewareApollo);

// loading routes
app.use(routes);

// loading error middleware
app.use(middlewareError);

var server = app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});

module.exports = server;

